<?php

namespace Mbs\HomeMaterial\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Layout;

class ActivityList implements ArgumentInterface
{ 
  private $attributeRepository;

  public function __construct(
    \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
  ) {
    $this->attributeRepository = $attributeRepository;
  }

  public function getActivityAttribute()
  {
    return $this->attributeRepository->get('activity');
  }

  public function getActivityListOrderedAlphabetically()
  {
    $options = $this->attributeRepository->get('activity')->getOptions();

    $result = [];
    foreach ($options as $info) {
      $result[$info->getlabel()] = $info;
    }

    ksort($result);

    return $result;
  }
}